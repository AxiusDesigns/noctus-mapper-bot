# NOCTUS MAPPERS DISCORD BOT

## Prerequisites
The following are required in order to run the Initialize file and the bot itself -
- Node.JS v8.11.4 ([DOWNLOAD](https://nodejs.org/en/download/) | [CENTOS](https://nodejs.org/en/download/package-manager/#enterprise-linux-and-fedora) | [UBUNTU](https://nodejs.org/en/download/package-manager/#enterprise-linux-and-fedora))
- Discord.js v11.4.2 ([INSTALLATION GUIDE](https://discord.js.org/#/docs/main/stable/general/welcome) (SEE INSTALLATION))
- Stable internet connection

## Installation
1. Invite the bot to the Server (Noctus Mappers) and the retrieve server (Noctus).
2. Run the "initjs" file to create the data files and the editable configuration file.
3. Edit the generated "config.json" file, changing all the "CHANGEME" values.
    - Don't know how to fill in ID fields? Turn on developer mode and right-click the servers/channels applicable to the value you wish to change and click "Copy ID".
4. Run the "main.js" file to start the bot. If you get any console errors, please report them to the bot developer (Fubbo) and they will assist you
5. Initialise the setup procediure through the "setup" command.
    - Don't know what to put? The bot developer will assist you in what to put.

## Bot Checklist
- [x] Initialization file
- [x] Utilities
- [x] Settings
- [x] Rank Requests
- [x] Suggestion Requests
- [x] Muting
- [x] Nonconformity and/or muted channels
- [ ] Ticket System
- [ ] \(OPTIONAL) Verification
- [ ] \(OPTIONAL) Coin System
- [ ] \(OPTIONAL) Coin Store
- [ ] \(OPTIONAL) Noctus Discord <-> NMDiscord Link
- [ ] \(OPTIONAL) Buycraft <-> Discord Ranking
- [ ] \(OPTIONAL) Gambling
- [ ] \(OPTIONAL) Plot Submissions
- [ ] \(OPTIONAL) Cross Server Punishments

## License Agreement
The license agreement can be found here:
1. [AxiusDesigns Site](https://www.axius.design/licenses/index.php?license=software)
2. [Raw License](https://www.axius.design/licenses/raw/software.txt)
3. [Raw File Download](https://www.axius.design/licenses/download.php?license=software)

By running the `init.js` file, running the `main.js` file or inviting / using the bot on your server(s), you automatically agree to the terms of service of the bot and the license agreement that is linked within the bot's source files. If you disagree with the terms of conditions / license, a custom license draft CAN be made for you, however theres a higher chance of the rights to use the software being restricted from you (the user).